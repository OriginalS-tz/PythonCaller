## Python Caller
use cpp to call python module

## How to use

#### use Arg type to build the argument list
```
template <typename _T1=std::string, typename _T2=std::string>
using Arg = std::vector<std::pair<_T1, _T2>>;
```

`PyObject *Py_BuildValue(char *format, ...);`

[char *format]
- "s" (string) [char *]
- "i" (integer) [int]

more in https://docs.python.org/3/extending/extending.html

#### Example

```cpp
Arg<std::string, int> args1 {{"i", 123}, {"i", 345}};
```

## Test Code
```cpp
PythonCom f;
if (!f.initModule("pytest"))
    return -1;
Arg<std::string, const char *> kkk {{"s","hello"}};

Arg<std::string, int> fff {{"l", 123}};
f.callFunc("foo", kkk);
f.callFunc("foo", fff);
f.close();
if (!f.initModule("test2"))
    return -1;
return 0;
```
