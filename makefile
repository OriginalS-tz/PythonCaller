#variable
object = main.o pycaller.o
pythonVersion = python2.7
Python_I = /usr/include/$(pythonVersion)
Python_L = /usr/lib64/$(pythonVersion)/config
depence = -I$(Python_I) -L$(Python_L) -l$(pythonVersion)
std = -std=c++11
#generate
ALL: callpy clean
callpy: $(object)
	g++ -o callpy $(object) $(depence)
main.o:
	g++ $(std) -c main.cpp $(depence)
pycaller.o:
	g++ $(std) -c pycaller.cpp $(depence)
clean:
	rm *.o
