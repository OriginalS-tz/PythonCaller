#include "pycaller.h"

int main() {
    PythonCom f;
    if (!f.initModule("pytest"))
        return -1;
    Arg<std::string, const char *> kkk {{"s","hello"}};
    Arg<std::string, int> fff {{"l", 123}};
    f.callFunc("foo", kkk);
    f.callFunc("foo", fff);
    f.close();
    if (!f.initModule("test2"))
        return -1;
    return 0;
}
