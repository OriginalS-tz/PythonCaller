#ifndef PYCALLER_H_
#define PYCALLER_H_
#include <iostream>
#include <Python.h>
#include <string>
#include <vector>
#include <utility>
template <typename _T1=std::string, typename _T2=std::string>
using Arg = std::vector<std::pair<_T1, _T2>>;

class PythonCom {
private:
    struct PyValue {
        PyObject *pName,*pModule,*pDict,*pFunc,*pArgs;
    } pyValue;
    bool initPython();
public:
    void close();
    PythonCom() {initPython();};
    bool initModule(std::string fileName);
    template<typename T>
    bool callFunc(std::string funcName, Arg<std::string, T> args);
    ~PythonCom() {close();Py_Finalize();};
};
#endif

template<typename T>
bool PythonCom::callFunc(std::string funcName, Arg<std::string, T> args) {
    pyValue.pFunc = PyDict_GetItemString(pyValue.pDict, funcName.c_str());
    if ( !pyValue.pFunc || !PyCallable_Check(pyValue.pFunc) ) {
        std::cout << "fail to call " << funcName << std::endl;
        getchar();
        return false;
     }
    pyValue.pArgs = PyTuple_New(1);
    int count = 0;
    for (auto i : args) {
        std::string Type = std::get<0>(i);
        T value = std::get<1>(i);
        std::get<0>(i);
        PyTuple_SetItem(pyValue.pArgs, count++, Py_BuildValue(Type.c_str(),value));
    }
    PyObject_CallObject(pyValue.pFunc, pyValue.pArgs);
    return true;
}
