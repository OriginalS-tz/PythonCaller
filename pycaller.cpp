#include "pycaller.h"

bool PythonCom::initPython() {
    Py_Initialize();
    if ( !Py_IsInitialized() ) {
        std::cout << "fail to initialize" << std::endl;
        return false;
    }
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append('./')");
}

void PythonCom::close() {
    Py_DECREF(pyValue.pName);
    Py_DECREF(pyValue.pArgs);
    Py_DECREF(pyValue.pModule);
}

bool PythonCom::initModule(std::string fileName) {
    pyValue.pName = PyString_FromString(fileName.c_str());

    pyValue.pModule = PyImport_Import(pyValue.pName);
    if ( !pyValue.pModule ) {
        return false;
    }
    pyValue.pDict = PyModule_GetDict(pyValue.pModule);
    if ( !pyValue.pDict ) {
        return false;
    }
    return true;
}
